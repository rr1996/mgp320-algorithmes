# -*- coding: utf-8 -*-
"""
Éditeur de Spyder

Ceci est un script temporaire.
"""

import zipf_gen_v2 as zfg

# Item storage
item_storage = []

#flow_size = [i*10 for i in range(1,100)]
flow_size = 1000
   
with open("generated_flows_alpha.txt","w") as f:
    # Instantiate the generator
    for alpha in range(-3, 6):
        zipfg = zfg.ZipfGen(9,alpha)

        for i in range(flow_size):
            f.write(str(zipfg.draw()))
#            current_flow.append(zipfg.draw())
        
        f.write("\n")
#        # Save current flow items
#        item_storage.append(current_flow)
