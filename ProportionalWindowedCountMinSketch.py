#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 09:21:03 2020
TODO : Do not convert to int during the mean calculation
@author: Rahenda Rosmedi
"""

from CountMinSketch import CountMinSketch

import copy

'''
Inheritance is inspired by this post :
https://www.digitalocean.com/community/tutorials/understanding-class-inheritance-in-python-3
'''
class ProportionalWindowedCountMinSketch (CountMinSketch):
    
    def __init__(self, relative_error=None, error_probability=None, 
                 input_data=None, window_size=10) :
        '''
        Extend CMS class
        '''
        
        super().__init__(relative_error, error_probability, input_data)
        
        # This is used to track the current item within the window
        self.pointer = 0
        
        self.window_size = window_size
        
        # snapshot sketch is initaly [0] and dimension(sketch) = dimension(snapshot)
        self.snapshot = copy.deepcopy(self.sketch)
        
        
    def update(self, item, count=1):
        '''
        Update the CMS by taking into account N (size of the window)
        TODO : Only count=1 is supported
        '''
        
        # If read items reach the end of the window_size, divide all sketch items
        # by the window_size.
        if self.pointer == self.window_size:
            self.snapshot = self.sketch/self.window_size
            self.pointer = 0
        
        self.pointer += 1
        
        # Update the counter of item count(i,j) = count(i,j) - snapshot(i,j)
        for i in range(self.depth):
            item_hash = self._hash_item(item, i)
            self.sketch[i][item_hash] += count
        
        self.sketch = self.sketch - self.snapshot
    
    def __str__(self):
        res = ''' Sketch depth (i.e. number of hash functions) : {0}
        \n Sketch width (i.e. number of hash function possible values) : {1}
        \n CM Sketch snapshot : \n {2} 
        '''.format(len(self.sketch), len(self.sketch[0]), self.sketch)
        return (res)