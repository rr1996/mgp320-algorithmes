#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 14:55:24 2020

@author: Rahenda Rosmedi
"""

# Prime number obtained from : https://www.numberempire.com/primenumbers.php
PRIMARY_NUMBER = 232049853957457

import numpy as np
import math
import random

class CountMinSketch (object):
    
    def __init__(self, relative_error=None, error_probability=None, 
                 input_data=None, width=None, depth=None, 
                 hash_functions_constants=None) :
        '''
        To instanciate this class, either provide 
        epsilon (relative_error) and delta (error_probability) -recommended-
        or
        width and depth
        Notes : input_data = a vector
        '''
        
        if relative_error is None :
            self.relative_error = 0.001
        else :
            self.relative_error = relative_error
        
        if error_probability is None :
            self.error_probability = 0.0001
        else :
            self.error_probability = error_probability
        
        if width is None :
            self.width = math.ceil(math.exp(1)/self.relative_error)
        else :
            self.width = width
        
        if depth is None :
            self.depth = math.ceil(math.log(1/self.error_probability, math.exp(1)))
        else :
            self.depth = depth
        
        if hash_functions_constants is None :
            self.hash_functions_constants = self._default_hash_function_constants()
            
        self.sketch = np.array([[0]*self.width]*self.depth, dtype=float)
        
        if input_data is None :
            self.input_data = None
        else :
            self.input_data = input_data
        
    def _default_hash_function_constants(self):
        '''
        TODO : use array instead of pointers list ?
        Our objectif is to generate here as many hash functions as value of depth
        We use here integer universal hash function using Carter-Wegman
        References : https://fr.wikipedia.org/wiki/Hachage_universel#Hachage_d'entiers
        '''
        hash_function_constants = []
        
        for i in range(self.depth):
            a = random.randrange(0, PRIMARY_NUMBER)
            b = random.randrange(0, PRIMARY_NUMBER)
            hash_function_constants.append([a, b])
        
        return np.array(hash_function_constants)
    
    def _hash_item(self, item, index):
        '''
        Map item into the hash functions universe
        We use here integer universal hash function using Carter-Wegman
        References : https://fr.wikipedia.org/wiki/Hachage_universel#Hachage_d'entiers
        item : the item to be hashed
        index : the index to indicate which hash function to use
        '''
        a = self.hash_functions_constants[index][0]
        b = self.hash_functions_constants[index][1]
        
        item_hash = ((a * item + b) % PRIMARY_NUMBER) % self.width
        
        return item_hash
        
    def update(self,item, count=1):
        '''
        Update the CMS
        An update is represented by a pair (it, ct)
        '''
        for i in range(self.depth):
            self.sketch[i][self._hash_item(item, i)] += count
        
    def estimate(self, item):
        '''
        Submit a point query of an item
        Here the estimator with the smallest value is returned
        '''
        item_estimators = []
        
        for i in range(self.depth):
            item_estimators.append(self.sketch[i][self._hash_item(item, i)])
        
        return (min(item_estimators))

    def count_input_data(self):
        '''
        count items in the self.input_data parameter
        '''
        # If there is an input vector, then begin the count
        for item in self.input_data :
            self.update(item)
    
    def __str__(self):
        res = ''' Sketch depth (i.e. number of hash functions) : {0}
        \n Sketch width (i.e. number of hash function possible values) : {1}
        \n CM Sketch snapshot : \n {2} 
        '''.format(len(self.sketch), len(self.sketch[0]), self.sketch)
        return (res)
    