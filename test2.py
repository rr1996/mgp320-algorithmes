#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 17:59:04 2020

@author: Rahenda Rosmedi - Joseph FACELINA

This test is to generate a graph Error(k)
It launches x executions on the same flow by varying k
"""
import numpy as np
import matplotlib.pyplot as plt

import Topkapi as algo
import NaiveCounter as nc

FLOWS_FILE = "2_flow_3000_items_0_to_100_alpha_1.txt"

# x and y axis
error = []
list_of_k = []

# the different flows
flows = []
flow_int = []

# Open the flows file and retrieve all flows
with open(FLOWS_FILE, "r") as f:
    list_of_items = f.readlines()
    
    # Convert the flows to list
    for g in list_of_items :
        # Convert to list
        an_item = int(g)
        
        flow_int.append(an_item)
    flows.append(flow_int)
    

for each_flow in flows :
    # Instanciate the Topkapi sketch
    for k in range(20,1000) :
        param_k = k
        param_n = 3000
        sketch = algo.Topkapi(0.1, 0.001, window_size=param_n, k=param_k)
        
        # Instanciate the naive counter
        naive_counters = nc.NaiveCounter()
        
        # Begin flow inspection
        for item in each_flow :
            sketch.update(item)
        
        # Compute the heavy hitters    
        sketch.compute_heavy_hitters()
    
        # Count last N elements with the naive counter to have the real result 
        for item1 in each_flow[-param_n:] :
            naive_counters.count(item1)
    
        # Get all results
        topkapi_result = sketch.get_heavy_hitters()
        real_counters = naive_counters.get_counters()
        print(" ... : " + str(topkapi_result))
        print(" +++ : " + str(real_counters))
        print(" THRESHOLD " + str (param_n/param_k))
        
        
        
        real_topk_indexes = np.where(real_counters[:]['lhh_count'] > (param_n/param_k))
        real_topk = real_counters[real_topk_indexes]
        #print(real_counters)
        # Compute the false positive result for the current flow
        error.append(abs((real_topk.shape[0] - topkapi_result.shape[0])/real_topk.shape[0]))
        
        # Count how many elements in this flow
        list_of_k.append(k)
    
plt.plot(list_of_k, error)


plt.show()
