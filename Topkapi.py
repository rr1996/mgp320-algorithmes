#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 09:22:06 2020

@author: Rahenda Rosmedi
"""
from ProportionalWindowedCountMinSketch import ProportionalWindowedCountMinSketch
import numpy as np

class Topkapi (ProportionalWindowedCountMinSketch):
    def __init__(self, relative_error=None, error_probability=None, 
                 input_data=None, window_size=10, k=2):
        '''
        Extend Proportional window CMS class
        threshold = {1...N} | N is the number of elements in the stream
        e.g. threshold = 2 --> 50% of all elements
        '''
        super().__init__(relative_error, error_probability, input_data, window_size)
        
        # A 2D array (Depth*Width) where each cell contains (LHH_count_ij, LHH_ij)
        # cell[0] : LHH_count_ij ; cell[1] : LHH_ij
        #self.sketch = np.zeros((2,3),dtype=[('lhh','float'), ('lhh_count','float')])
        self.sketch = np.zeros((self.depth, self.width, 2), dtype=float)

#        self.sketch = np.array([[0]*self.width]*self.depth, dtype=object)       
#        self.sub_cells = np.zeros((2))
#        # set all counters to zero
#        self.__reset_counters()
        
        # threshold defines the quantile (https://www.thoughtco.com/what-is-a-quantile-3126239)
        self.threshold = float(window_size/k)
        self.k = k
        
        # Will be populated when sending the heavy hitters
        self.candidate_set = np.array([],dtype=[('lhh_count','float'), ('lhh','int')])
        
        self.snapshot = np.copy(self.sketch)
        
        
    def update(self, item, count=1):
        '''
        Update the sketch according to Topkapi algorithm
        '''
        # If pointer reach the end of the window_size,
        # divide all counters by the window_size and reset the pointer.
        if self.pointer == self.window_size:
            self.snapshot = np.copy(self.sketch) # same as copy.deepcopy
            self.snapshot[:, :, 0] /= self.window_size
            self.pointer = 0
        
        self.pointer += 1
        
        for i in range(self.depth):
            item_hash = self._hash_item(item, i)
            
            sub_cells = self.sketch[i][item_hash]
            
            # If the incoming item correspond to the locally most frequent item
            # Then increment the counter, otherwise decrement it.
            if sub_cells[1] == item :
                self.sketch[i][item_hash][0] += 1
            else :
                self.sketch[i][item_hash][0] -= 1
                if sub_cells[0] < 0 :
                    self.sketch[i][item_hash][1] = item
                    self.sketch[i][item_hash][0] = 1
       
        
        # Decrease all counters by the snapshot values
        self.sketch[:,:,0] -= self.snapshot[:,:,0]
                    
                    
    def compute_heavy_hitters(self):
        '''
        Compute the heavy hitters where counter > threshold
        '''
        # Reset candidate_set
        self.candidate_set = np.array([],dtype=[('lhh_count','float'), ('lhh','int')])
        
        for j in range(self.width):
            
            item_0j = self.sketch[0][j][1]
            item_counter_0j = self.sketch[0][j][0]
            
            if item_counter_0j > self.threshold :
                # If the first counter is greater than threshold, add the subcells to the CS
                self.candidate_set = np.insert(self.candidate_set, len(self.candidate_set), tuple(self.sketch[0][j]))
            else :
                # If the first counter is not greater than the threshold,
                # check if any other counter of the item is greater than the threshold
                for i in range(1, self.depth) :
                    item_hash = self._hash_item(int(item_0j), i)
                    item_ij = self.sketch[i][item_hash][1]
                    item_counter_ij = self.sketch[i][item_hash][0]
                    
                    if item_counter_ij > self.threshold :
                        # If we found the item's counter greater than the threshold, add it to CS
                        # and stop searching for the other item's counters
                        if item_ij == item_0j:
                            self.candidate_set = np.insert(self.candidate_set, self.candidate_set.shape[0], tuple(self.sketch[i][item_hash]))
                            break # not sure of this but it's logical, otherwise there will be lots of duplicates
#        print(" DEBUG 1 ")

#        print(" DEBUG 2 ")
        # Quick sort CS according to the counters
        self.candidate_set = np.sort(self.candidate_set,kind='mergesort', order="lhh_count")
#        print("----------------CS sorted----------------")
#        print(self.candidate_set)
#        print(" DEBUG 3 ")
#        print("----------------Top K in ascending order----------------")
#        print(self.candidate_set[-self.k:])
#        # Return the top-k element
#        return(self.candidate_set[-self.k:])
    
    
    def get_heavy_hitters(self):
        '''
        Return the heavy hitters where counter > threshold
        '''
        return(self.candidate_set[:])
        
        


        